**Implementation of 3 design patterns in python:**


1. Singleton is a creational design pattern that lets you ensure that a class has 
only one instance, while providing a global access point to this instance.


2. Decorator is a structural design pattern that lets you attach new behaviors to 
objects by placing these objects inside special wrapper objects that contain 
the behaviors.


3. Mediator is a behavioral design pattern that lets you reduce chaotic 
dependencies between objects. The pattern restricts direct communications
between the objects and forces them to collaborate only via a mediator object.